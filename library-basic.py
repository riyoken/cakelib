import socket, time, random, re, sys, select, threading, json, html.entities, _thread, urllib.parse, queue, imp, glob
import urllib.request as urlreq
from urllib.parse import quote, unquote
from threading import Thread

w12 = 75
sv2 = 95
sv4 = 110
sv6 = 104
sv8 = 101
sv10 = 110
sv12 = 116
tagserver_weights = [["5", w12], ["6", w12], ["7", w12], ["8", w12], ["16", w12], ["17", w12], ["18", w12], ["9", sv2], ["11", sv2], ["12", sv2], ["13", sv2], ["14", sv2], ["15", sv2], ["19", sv4], ["23", sv4], ["24", sv4], ["25", sv4], ["26", sv4], ["28", sv6], ["29", sv6], ["30", sv6], ["31", sv6], ["32", sv6], ["33", sv6], ["35", sv8], ["36", sv8], ["37", sv8], ["38", sv8], ["39", sv8], ["40", sv8], ["41", sv8], ["42", sv8], ["43", sv8], ["44", sv8], ["45", sv8], ["46", sv8], ["47", sv8], ["48", sv8], ["49", sv8], ["50", sv8], ["52", sv10], ["53", sv10], ["55", sv10], ["57", sv10], ["58", sv10], ["59", sv10], ["60", sv10], ["61", sv10], ["62", sv10], ["63", sv10], ["64", sv10], ["65", sv10], ["66", sv10], ["68", sv2], ["71", sv12], ["72", sv12], ["73", sv12], ["74", sv12], ["75", sv12], ["76", sv12], ["77", sv12], ["78", sv12], ["79", sv12], ["80", sv12], ["81", sv12], ["82", sv12], ["83", sv12], ["84", sv12]]

specials = {'mitvcanal': 56, 'magicc666': 22, 'livenfree': 18, 'eplsiite': 56, 'soccerjumbo2': 21, 'bguk': 22, 'animachat20': 34, 'pokemonepisodeorg': 55, 'sport24lt': 56, 'mywowpinoy': 5, 'phnoytalk': 21, 'flowhot-chat-online': 12, 'watchanimeonn': 26, 'cricvid-hitcric-': 51, 'fullsportshd2': 18, 'chia-anime': 12, 'narutochatt': 52, 'ttvsports': 56, 'futboldirectochat': 22, 'portalsports': 18, 'stream2watch3': 56, 'proudlypinoychat': 51, 'ver-anime': 34, 'iluvpinas': 53, 'vipstand': 21, 'eafangames': 56, 'worldfootballusch2': 18, 'soccerjumbo': 21, 'myfoxdfw': 22, 'animelinkz': 20, 'rgsmotrisport': 51, 'bateriafina-8': 8, 'as-chatroom': 10, 'dbzepisodeorg': 12, 'tvanimefreak': 54, 'watch-dragonball': 19, 'narutowire': 10, 'leeplarp': 27}

startbyte = b'\x00'
endbyte = b'\r\n\x00'

class derp(object):
    def __init__(self):
            self.name = 'user'
            self.password = 'pass'  
            self._uid = str(int(random.randrange(1000000000000000, 10000000000000000)))
            self._byte = b''
            self.connected = False
            self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.pm_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self._ping = 11111
            self.postbyte = False
            self.pm_postbyte = False
            self._message = None
            self.sockets = dict()
            self.chat_byte = b''

    def connect(self, rooms):
            i = [x for x in rooms]
            list(map(lambda x: self.room_connect(x), i))
            running = True
            _thread.start_new_thread(self.ping, (self._ping,))
            _thread.start_new_thread(self.pm_connect,('illogic',))
            while running:
                connections, roomname = self.gConnections()
                readers = [x for x in connections] 
                writers = [x for x in connections if self._byte != b""]
                rlist,wlist,xlist = select.select(readers,writers,[],0.2)
                for socketRead in rlist:
                    roomnames = [x for x in roomname if self.sockets.get(x) == socketRead]
                    try:
                        data = socketRead.recv(1024)
                    except: pass
                    if len(data) > 0:
                        byte = b''
                        data += byte
                        self.event_data(data, roomnames)
                        byte = b''

    def gConnections(self):
        connections = list(self.sockets.values())
        roomname = list(self.sockets.keys())
        return connections, roomname

    def room_connect(self, rooms):
        try: derp = self.sockets[rooms]
        except KeyError:
            host = getServer(rooms)
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            sock.connect((host, 443)) 
            sock.send(self.room_auth(rooms).encode())
            self.sockets.update({rooms:sock})
            print('connected to '+ rooms)
            self.postbyte = True

    def start(self, rooms):
        self.connect(rooms)

    def pm_connect(self, args):
        self.pm_sock.connect(("c1.chatango.com", 5222))
        self.pm_sock.send(str("tlogin:"+ pm_auth(self.name, self.password)+ ":2"+"\x00").encode())
        _thread.start_new_thread(self.pm_ping, (self._ping,))
        self.postbyte = True
        running = True
        while running:
            data = b''
            data += self.pm_sock.recv(3000)
            self.event_data(data, roomname = 'pm')
            data = b''

    def event_data(self, item, roomname):
       self.connected = True
       data1 = item
       data1 = data1.split(b'\x00')
       list(map(lambda cake: self.event_decode(cake, roomname), data1))

    def event_decode(self, cake, roomname):
       data = cake.decode('latin-1').rstrip('\r\n')
       data = data.rstrip('\n').rstrip('\r').strip('\n').strip('\r')
       data = data.split(":")
       event = data[0]
       args = data[1:]
       self.event_recv(event, args, roomname)

    def pm_event_data(self):
       while not self._byte.endswith(startbyte):
           self.pm_connected = True
           data = self.pm_sock.recv(3000)
           data = data.strip(b'\r\n\x00').decode()
           event = data.split(":")[0]
           args = data.split(":")[1:]
           self.event_recv(event, args, roomname = '')

    def room_auth(self, room):
            ret = 'bauth:' + room + ':' + self._uid + ':' + self.name + ':' + self.password + '\x00'
            return ret

    def ping(self, pingid):
      time.sleep(60)
      while self.connected and pingid == self._ping:
        self.send("")
        time.sleep(60)
        
    def pm_ping(self, pingid):
      time.sleep(10)
      while self.connected and pingid == self._ping:
        self.pm_sock.send(b"\r\n\x00")
        time.sleep(10)


    def event_recv(self, event, args, roomname):
        print(event, args)

    def send(self,*args):
        if not self.postbyte:
            terminator = b'\x00'
        else:
            terminator = b'\r\n\x00'
            data = ':'.join(args).encode()
            try:
                for item in list(self.sockets.values()):
                    item.send(data+terminator)
            except:
                for x in room_list: ### linux server does not like the threads possibly fix this so it wont need to reconnect if the pipe is broken
                    self.room_connect(x)

    def pm_send(self,*args):
            terminator = b'\r\n\x00'
            data = ':'.join(args).encode()
            self.pm_sock.send(data+terminator)


def getServer(group):
  def n_server(group):
        group = group.replace("_", "q")
        group = group.replace("-", "q")
        lcv10 = min(5, len(group))
        lcv12 = int(group[:lcv10], 36)
        if len(group) > 6:
          lcv11 = group[6:][:min(3, len(group) - 5)]
          lcv8 = int(lcv11, 36)        
        else: lcv8 = 1000
        lcv8 = 1000 if lcv8 <= 1000 or lcv8 == None else lcv8
        num = (lcv12 % lcv8) / lcv8
        limit = sum(map(lambda x: x[1], tagserver_weights))
        cake = 0
        stop = 0
        s_number = 0
        for x in tagserver_weights:
          cake += float(x[1]) / limit
          if(num <= cake) and s_number == 0:
            s_number += int(x[0])
        return s_number
  try:
    sn = str(specials[group])
  except KeyError:
    sn = str(n_server(group))
  return "s{}.chatango.com".format(sn)

def pm_auth(u, p):
    '''username, password to return authentication code'''
    toencode = urllib.parse.urlencode({'user_id': u, 'password': p, 'storecookie': 'on', 'checkerrors': 'yes'}).encode()
    request = urllib.request.urlopen('http://chatango.com/login', toencode)
    data = request.headers
    auth = re.compile(r'auth.chatango.com=(.*?);', re.IGNORECASE).search(str(data)).group(1)
    if auth != '': return auth
    else: raise ValueError('invallid username/password')

room_list = ['multi-bot','way-of-the-nigga']

if __name__ == "__main__":
    derp().start(room_list)
