##rough draft for the pvp, not at all done

def bp_add(user):
        bp = 1
        try: bp += int(bot_points[user])
        except KeyError: bp = bp
        bot_points[user] = json.dumps(bp)
        json_write('bp',bot_points)

def fuck_me(user, points):
        bp = int(points)
        try: bp += int(bot_points[user])
        except KeyError: bp = bp
        bot_points[user] = json.dumps(bp)
        json_write('bp',bot_points)

def points(x, user, uid, rank, roomname):
        try: points = 'they have ' + str(bot_points[x]) if len(x) != 0 else 'you have ' + str(bot_points[user])
        except: points = 'they have no' if len(x) != 0 else 'you have no'
        return points + ' points'

def bp_subtract(user, points):
        try: nbp = int(json.loads(bot_points[user])) - int(points)
        except KeyError:
                nbp = 0
                nbp -= int(points)
        bot_points[user] = json.dumps(nbp)
        json_write('bp',bot_points)


def attack(string, user, uid, rank, roomname):
        try:
                try: team, timeset = json.loads(teams[user])
                except KeyError: return 'you must register with a team before you can attack'
                recv, atk = string.split(' ',1)
                try:
                        toatk = atk.split('with')[1]
                        toatk = toatk.split(' ',2)
                        null, weapon, atk = toatk                        
                except: atk, weapon = atk, 'n'
                u_points = int(json.loads(bot_points[user]))
                health = get_health(user)
                atk = int(atk)
                try: r_team, timeset = json.loads(team_set[recv])
                except KeyError: return 'that person is not yet registered with a team'
                if atk < 0: return 'you cant attack with negative points'
                elif r_team == team: return 'you cant attack a member of your own team'
                elif health <= 0: return 'you have died, you must wait untill you regain health.
                elif atk <= u_points:
                        amount, dtype = get_damage_total(int(atk), team, weapon, user)
                        chance = random.randrange(0,4)
                        _min = amount / 2
                        _max = amount * 3 if chance == 2 else amount * 2
                        _min = str(_min).split('.')[0] if '.' in str(_min) else _min
                        _max = str(_max).split('.')[0] if '.' in str(_max) else _max
                        _min, _max = int(_min), int(_max)
                        damage = random.randrange(_min, _max)
                        cash_bonus = damage*.2
                        cash_add(user, cash_bonus)
                        if dtype == 'h': health_subtract(recv, damage)
                        elif dtype == 'a': bp_subtract(recv, damage)
                        bp_subtract(user, atk)                        
                        return 'attacked {a} for {b} points'.format(a=recv, b=damage)
                else: return 'you dont have enough points or you tried to attack with a number lower than 0'
        except Exception as e: return '%s' % e

def get_damage_total(atk, weapon, team, user):
        if weapon == 'n':
                dtype = 'h'
                damage = atk
                captain, damagemult, healthmult, members, base = json.loads(teams[team])
                damage *= damagemult
                total_damage = damage
                
        else:
                dtype, damage, ammo, cost, firerate = pvp_items[user][weapon]
                damage += atk
                damage *= firerate
                captain, damagemult, healthmult, members, base = json.loads(teams[team])
                damage *= damagemult
                total_damage = damage
        return [total_damage, dtype]

def get_health(user):
        team, timeset = json.loads(team_set[user])
        captain, damagemult, healthmult, members, base = json.loads(teams[team])
        health, healthset = json.loads(player_status[user])
        health = float(health) if '.' in health else int(health)
        health += 20000 if time.time() - float(healthset) > 1800 else heath
        n_health = float(healthmult)*health if health > 0 else health
        healthset = time.time()
        player_status[user] = json.dumps([health, healthset])
        return n_health

def heal(string, user, uid, rank, roomname):
        _healnum = 90
        healnum = 1500
        health_ = random.randrange(_healnum, healnum)
        try: health, healthset = json.loads(player_status[user])
        except KeyError: 'you must register with a team first'
        if time.time() - float(healthset) < 90: return 'you can only heal once every 1.5 minutes'
        else:
                health += health_
                player_status[user] = json.dumps([health, healthset])
                return 'healed'
        
def team(string, user, uid, rank, roomname):
        action, function = string.split(' ',1).lower()
        teamlist ['blue','red','green','black','white','purple','pink', 'lemons']
        if action == 'change':
                try:
                        try: team, timeset = json.loads(team_set[user])
                        except KeyError: return 'you have to register with a team before you can change silly'
                        if time.time() - int(timeset) < 86400: return 'you can only change teams once a day'
                        elif team not in teamlist: return 'that is not a valid team, please refer to the teamlist'
                        elif team == function: return 'you are already in that team'
                        else:
                                timeset = time.time()
                                team_n = function
                                team_set[user] = json.dumps([team_n, timeset])
                                captain, damagemult, healthmult, members, base = json.loads(teams[team_n])
                                members = int(members) + 1
                                teams[team_n] = json.dumps([captain, damagemult, healthmult, members, base])
                                captain, damagemult, healthmult, members, base = json.loads(teams[team])
                                members = int(members) - 1
                                teams[team] = json.dumps([captain, damagemult, healthmult, members, base])
                                player_status[user] = json.dumps([20000, time.time()])
                                return 'you are now in team '+team
                except Exception as e: return '%s' % e
        elif action == 'status':
                try:
                        team = function
                        captain, damagemult, healthmult, members, base = json.loads(teams[team])
                        return '<br><br>team: %s<br>captain: %s<br>damage X: %s<br>health X: %s<br>members: %s<br>home base: %s' % (team, captain, damagemult, healthmult, members, base)
                except KeyError: return 'that is not a valid team'
        elif action == 'register':
                try:
                        team, timeset = json.loads(team_set[user])
                        return 'you are already registered with a team silly, try team change'
                except KeyError:
                        
                        timeset = time.time()
                        team = function
                        if team not in teamlist: return 'that is not a valid team'
                        else:
                                team_set[user] = json.dumps([team, timeset])
                                captain, damagemult, healthmult, members, base = json.loads(teams[team])
                                members = int(members) + 1
                                teams[team] = json.dumps([captain, damagemult, healthmult, members, base])
                                player_status[user] = json.dumps([20000, time.time()])
                                return 'you are now in '+team+' team.'


def weapon(string, user, uid, rank, roomname):
        try: action, function = string.split(' ',1).lower()
        except: action = string.lower()
        weapon_dict = {'pistol': ['h', 50, 150, 25, 1], 'daf': ['h', 23, 5, 95, 15], 'diadsfldo':['h', 6969, 2, 2500, 1], 'lazer gun':['h', 2000, 45, 675, 1], 'higgs boson':['a', 500, 10, 450, 1] }
        if action == 'buy':
                item = function
                try: dtype, damage, ammo, cost, firerate = weapon_dict[item]
                except KeyError: return 'that is not a valid item'
                money = json.loads(currecy[user])
                if cost > int(money): return 'you dont have enough money to buy that'
                else:
                        money -= cost
                        currency[user] = json.dumps(money)
                        try:
                                dtype, damage, nammo, cost, firerate = pvp_items[user][item]
                                ammo += int(nammo)
                                pvp_items[user] = {item:[dtype, damage, ammo, cost, firerate]}
                        except KeyError:
                                pvp_items[user] = {item:[dtype, damage, ammo, cost, firerate]}
                        return 'bought %s for %s' % (item, cost)
        if action == 'list':
                w_list = [x+': cost:'+str(weapon_dict[x][3])+' damage: '+str(weapon_dict[x][1])+' ammo: '+str(weapon_dict[x][2])+' firerate: '+str(weapon_dict[x][4])+' damage type: '+ str(weapon_dict[x][0]) for x in weapon_dict]
                return '<br><br>'+ '<br>'.join(w_list)

def item(string, user, uid, rank, roomname):
        action, function = string.split(' ',1).lower()
        item_dict = {}
        if action == 'buy':
                item = function
                try: dtype, amount, inventory, cost, duration = item_dict[item]
                except KeyError: return 'that is not a valid item'
                money = json.loads(currency[user])                
                if cost > int(money)): return 'you dont have enough money to buy that'
                else:
                        money -= cost
                        currency[user] = json.dumps(money)
                        try:
                                dtype, amount, ninventory, cost, duration = pvp_items[user][item]
                                inventory += int(ninventory)
                                pvp_items[user] = {item:[dtype, amount, inventory, cost, duration]}
                        except KeyError:
                                pvp_items[user] = {item:[dtype, amount, inventory, cost, duration]}
                        return 'bought %s for %s' % (item, cost)
