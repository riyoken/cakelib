import socket, time, random, re, sys, select, threading, json, html.entities, _thread, urllib.parse, queue, imp, glob, text
import urllib.request as urlreq
import core
from urllib.parse import quote, unquote
from threading import Thread
from random import shuffle
from text import uids, jsonrooms, room_list, lockrooms, ranks, levelname, rmind, lockrooms

#v 1.5
#by riyoken/monday :: version 1.0 completed on 8/24/2013 :: more events can be added as seen fit.
#just delete any core or text file related stuff to use, or link your code if it is similar
#username and password are loaded off of a util.txt, read the code to see how it works.

w12 = 75
sv2 = 95
sv4 = 110
sv6 = 104
sv8 = 101
sv10 = 110
sv12 = 116
tagserver_weights = [["5", w12], ["6", w12], ["7", w12], ["8", w12], ["16", w12], ["17", w12], ["18", w12], ["9", sv2], ["11", sv2], ["12", sv2], ["13", sv2], ["14", sv2], ["15", sv2], ["19", sv4], ["23", sv4], ["24", sv4], ["25", sv4], ["26", sv4], ["28", sv6], ["29", sv6], ["30", sv6], ["31", sv6], ["32", sv6], ["33", sv6], ["35", sv8], ["36", sv8], ["37", sv8], ["38", sv8], ["39", sv8], ["40", sv8], ["41", sv8], ["42", sv8], ["43", sv8], ["44", sv8], ["45", sv8], ["46", sv8], ["47", sv8], ["48", sv8], ["49", sv8], ["50", sv8], ["52", sv10], ["53", sv10], ["55", sv10], ["57", sv10], ["58", sv10], ["59", sv10], ["60", sv10], ["61", sv10], ["62", sv10], ["63", sv10], ["64", sv10], ["65", sv10], ["66", sv10], ["68", sv2], ["71", sv12], ["72", sv12], ["73", sv12], ["74", sv12], ["75", sv12], ["76", sv12], ["77", sv12], ["78", sv12], ["79", sv12], ["80", sv12], ["81", sv12], ["82", sv12], ["83", sv12], ["84", sv12]]

specials = {'mitvcanal': 56, 'magicc666': 22, 'livenfree': 18, 'eplsiite': 56, 'soccerjumbo2': 21, 'bguk': 22, 'animachat20': 34, 'pokemonepisodeorg': 55, 'sport24lt': 56, 'mywowpinoy': 5, 'phnoytalk': 21, 'flowhot-chat-online': 12, 'watchanimeonn': 26, 'cricvid-hitcric-': 51, 'fullsportshd2': 18, 'chia-anime': 12, 'narutochatt': 52, 'ttvsports': 56, 'futboldirectochat': 22, 'portalsports': 18, 'stream2watch3': 56, 'proudlypinoychat': 51, 'ver-anime': 34, 'iluvpinas': 53, 'vipstand': 21, 'eafangames': 56, 'worldfootballusch2': 18, 'soccerjumbo': 21, 'myfoxdfw': 22, 'animelinkz': 20, 'rgsmotrisport': 51, 'bateriafina-8': 8, 'as-chatroom': 10, 'dbzepisodeorg': 12, 'tvanimefreak': 54, 'watch-dragonball': 19, 'narutowire': 10, 'leeplarp': 27}

iconserver_prefix = "i0"
av_prefix = "m1"
usc_prefix = "st"
sc_prefix = "st"
psc_prefix = "pst"

startbyte = b'\x00'
endbyte = b'\r\n\x00'

repattern = "<[^>]+>"
repattern2 = r"<[^>]+>"

font_color = 'ffffff'
name_color = '00ffff'

rankError = 'you do not have the permission to do this'

opener = urlreq.build_opener()
opener.addheaders=[('User-agent', 'Mozilla/5.0')]

prefix = '*'

def translate(lang, msg):
    lin = ''
    lout = lang
    textin = msg
    url = "http://translate.google.fr/translate_a/t?client=a&text=%s&hl=%s&sl=%s&tl=%s&ie=UTF-8&oe=UTF-8&multires=1&ssel=0&tsel=0&sc=1" % (quote(textin),lin,lin,lout,)
    search_results = opener.open(url)
    rest = json.loads(search_results.read().decode())
    results = rest['sentences']
    for i in results: rest = i['trans']            
    return rest

def get_in():
    f = open('util.txt','r').read()
    info = re.compile(r'<p>(.*?)</p>', re.IGNORECASE).search(str(f)).group(1)
    info = deobfus(info)
    info = deobfus(info)
    return info

def get_us():
    f = open('util.txt','r').read()
    info = re.compile(r'<u>(.*?)</u>', re.IGNORECASE).search(str(f)).group(1)
    return info
    
def pm_auth(u, p):
    '''username, password to return authentication code'''
    toencode = urllib.parse.urlencode({'user_id': u, 'password': p, 'storecookie': 'on', 'checkerrors': 'yes'}).encode()
    request = urllib.request.urlopen('http://chatango.com/login', toencode)
    data = request.headers
    auth = re.compile(r'auth.chatango.com=(.*?);', re.IGNORECASE).search(str(data)).group(1)
    if auth != '': return auth
    else: raise ValueError('invallid username/password')
    
class derp(object):
    def __init__(self):
            self.name = get_us()
            self.password = get_in()            
            self._uid = str(int(random.randrange(1000000000000000, 10000000000000000)))
            self._byte = b''
            self.connected = False
            self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.pm_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self._ping = 11111
            self.postbyte = False
            self.pm_postbyte = False
            self._message = None
            self.room_mods = dict()
            self.room_owner = dict()
            self.sockets = dict()
            self.uid_dict = dict()
            self.banlist = dict()
            self.user_list = dict()
            self.chat_byte = b''
            self._lastmsg = dict()
            self.idle_dict = dict()
            self.msg_ids = dict()
            self._ips = dict()
            self.font = '<f x12{a}="9"><n{b}/>'.format(a=font_color, b=name_color)
            
    def connect(self, rooms):
            i = [x for x in rooms]
            list(map(lambda x: self.room_connect(x), i))
            running = True
            _thread.start_new_thread(self.ping, (self._ping,))
            _thread.start_new_thread(self.pm_connect,('illogic',))
            while running:
                connections, roomname = self.gConnections()
                readers = [x for x in connections] 
                writers = [x for x in connections if self._byte != b""]
                rlist,wlist,xlist = select.select(readers,writers,[],0.2)
                for socketRead in rlist:
                    roomnames = [x for x in roomname if self.sockets.get(x) == socketRead]
                    try:
                        data = socketRead.recv(1024)
                    except: pass
                    if len(data) > 0:
                        byte = b''
                        data += byte
                        self.event_data(data, roomnames)
                        byte = b''

    def gConnections(self):
        connections = list(self.sockets.values())
        roomname = list(self.sockets.keys())
        return connections, roomname

    def room_join(self, string, user, uid, rank, roomname):
        if rank > 2:
                if string in room_list:
                        return "kunfuzzle"
                elif checkg(string) == False:
                        return "that is not a real group"
                else:
                        host = getServer(string)
                        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                        sock.connect((host, 443)) 
                        sock.send(self.room_auth(string).encode())
                        self.sockets.update({string:sock})
                        self.record_room(string, user)
                        room_list.append(string)
                        print('connected to '+ string)
                        return 'joined ' + string
        else:
                return 'you do not have permission to join. contact a mod'
        

    def room_connect(self, rooms):
        try: derp = self.sockets[rooms]
        except KeyError:
            host = getServer(rooms)
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            sock.connect((host, 443)) 
            sock.send(self.room_auth(rooms).encode())
            self.sockets.update({rooms:sock})
            print('connected to '+ rooms)
            self.postbyte = True

    def ret_banlist(self, roomname):
        self.room_send("blocklist:block::next:500", roomname)

    def leave(self, string, user, uid, rank, roomname):
        if rank > 1:
            if string == 'here': name = ''.join(roomname)
            else: name = string
            del self.sockets[name]
            del jsonrooms[name]
            room_list.remove(name)
            f = open("jsonrooms.txt", "w")                                
            for x in jsonrooms:                                        
                    joiner, roomlock, jointime, locker, locktime = json.loads(jsonrooms[x])
                    f.write(json.dumps([x, joiner, roomlock, jointime, locker, locktime])+"\n")                                        
            f.close() 
            return 'left ' + string

    def pm_connect(self, args):
        self.pm_sock.connect(("c1.chatango.com", 5222))
        self.pm_sock.send(str("tlogin:"+ pm_auth(self.name, self.password)+ ":2"+"\x00").encode())
        _thread.start_new_thread(self.pm_ping, (self._ping,))
        self.postbyte = True
        running = True
        while running:
            data = b''
            data += self.pm_sock.recv(3000)
            self.event_data(data, roomname = 'pm')
            data = b''

    def event_data(self, item, roomname):
               self.connected = True
               data1 = item
               data1 = data1.split(b'\x00')
               list(map(lambda cake: self.event_decode(cake, roomname), data1))

    def event_decode(self, cake, roomname):
               data = cake.decode('latin-1').rstrip('\r\n')
               data = data.rstrip('\n').rstrip('\r').strip('\n').strip('\r')
               data = data.split(":")
               event = data[0]
               args = data[1:]
               self.event_recv(event, args, roomname)

    def pm_event_data(self):
            while not self._byte.endswith(startbyte):
               self.pm_connected = True
               data = self.pm_sock.recv(3000)
               data = data.strip(b'\r\n\x00').decode()
               event = data.split(":")[0]
               args = data.split(":")[1:]
               self.event_recv(event, args, roomname = '')
               msgbody = {event: args}

    def event_recv(self, event, args, roomname):
        if event == "ok":
             self.room_send("blocklist:block::next:500", roomname)
             self.send("getpremium","1")
             self.send("msgbg", "1")
             roomowner = ''.join(args[0])
             roommods = ', '.join(args[6].split(";"))
             self.room_mods.update({''.join(roomname): roommods})
             self.room_owner.update({''.join(roomname): roomowner})
            
        if event == "b":
             try:
                 msg = ":".join(args[9:])
                 message_body = re.sub(repattern, "", msg)
                 message_body = message_body.replace('http//','http://')
                 message_user = "".join(args[1])
                 message_uid = "".join(args[3])
                 if message_user == "":
                     _id = re.search("<n(.*?)/>", msg)
                     if _id: _id = _id.group(1)
                     message_user = "$" + args[2] if len(args[2]) != 0 else '@anon'+anon_id(_id, message_uid)
                 font = re.match(repattern2, msg)
                 if font != None:
                     try: font = font.group(1)
                     except: font = font.group(0)
                     if '/>' in font: font = font.split('/>')[1]
                     else: font = font
                     if len(font) != 0: message_font = font
                 message_ip = "".join(args[6])
                 message_id = "".join(args[4])
                 self.Message(message_body, message_user, message_uid, message_ip, message_id, roomname)
             except: pass
             

        if event == "msg":
            try:
                username1, username2, derp, time, number, msg = args
                message_body = re.sub(repattern, "", msg)
                message_user = username1
                self.PM_recv(message_body, message_user)
            except: pass

        if event == "blocked":
            self.ban_record('%'.join(args))

        if event == "blocklist":
            users = '%'.join(args).split(';')
            list(map(lambda x: self.ban_record(x, roomname), users))

        if event == "mods":
            roommods = ', '.join(args)
            self.room_mods.update({''.join(roomname): roommods})

        if event == 'g_participants':
            y = []
            details = [x.split('!') for x in '!'.join(args).split(";")]
            for data in details:
                x = []
                try:
                    sid = str(data[1]).split('.')[0]
                    sid = sid[6:]
                    uid = data[2]
                    temp = '$' + data[4]
                    name = data[3].lower() if data[3].lower() != 'none' else '@anon'+anon_id(sid, uid)
                    user = temp if temp != '$None' else name
                    lemons = ['$non','$no','$n']
                    user = user.lower()
                    if user not in lemons:
                        if user != '$': self.uid_record(user, uid)
                    x.append([''.join(roomname), user])
                    if x[0][0] == ''.join(roomname):
                        y.append(x[0][1])
                        self.user_list.update({''.join(roomname): y})
                except: continue

        if event == "OK":
            self.pm_send("wl")
            self.pm_send("getblock")
            self.pm_send("getpremium", "1")
            
        if event == "wl":
            list(map(lambda y: self.get_idle(args[y*4:y*4+4]), range(len(args)//4)))            

        if event == "miu":
            pass

        if event == 'participant':
                try:
                    if args[0] == '1' or '2':
                                self.room_send('g_participants:start', roomname)
                except: pass
            
        if event == 'inited':
           rlist = ' '.join(room_list)
           rlist = rlist.replace(' i3ryok','').replace(' deroleplaychat','')
           rlist = rlist.split(' ')
           list(map(lambda x: self.room_send('g_participants:start', x), rlist))

    def PM_recv(self, body, user):
             data = body.split(" ", 1)
             if len(data) > 1: cmd, string = data[0], data[1]
             else: cmd, string = data[0], ""
             func = cmd
             if hasattr(core, func):
                 try: self.pm_send('msg', user, getattr(core, func)(string, user, uid = '', rank = '', roomname = ''))
                 except: print('derp')
             else: self.pm_send('msg', user, 'hello, i am a bot. you can use my commands through the pm. although, all activites are monitored by moderators.')
                 
    def Message(self, body, user, uid, ip, mid, roomname):
             #print('body:',body)
             user = user.lower()
             rank = self._Rank(user, roomname)
             #print(body)
             if user in core.rmind.keys():
                 tmsg, suser, stime = json.loads(core.rmind[user])
                 self.post('message from {a}: {b} from {c} ago.'.format(a=suser,b=tmsg,c=core.getSTime(stime)), roomname)
                 core.rmind_remove(user)
             #print(body)
             data = body.split(" ", 1)
             if len(data) > 1: cmd, string = data[0], data[1]
             else: cmd, string = data[0], ""
             string = clean(string)
             #print(string)
             self.uid_record(user, uid)
             if len(ip) > 0:
                 self._ips.update({user: ip})
             self.msg_ids.update({user: mid})
             if len(cmd) > 0:
                 if cmd[0] == prefix:
                     _prefix = True
                     cmd = cmd[1:]
                 else: _prefix = False
                 func = cmd.lower()
                 #print(func, string)
                 if user != self.name:
                     core.seen_record(body, user, roomname)
                     if ''.join(roomname) in lockrooms and self._Rank(user, roomname) < 2: pass
                     else:
                         if user[0] not in ['@','$']:
                             if rank > -1:
                                 if func == 'illogic': self.post('yes '+user+'?', roomname)
                                 elif _prefix:
                                     #print('prestring:',string)
                                     self.call_message(func, string, user, uid, rank, roomname)

    def call_message(self, func, string, user, uid, rank, roomname):
             if hasattr(self, func):
                 try: self.post(getattr(self, func)(string, user, uid, rank, roomname), roomname)
                 except Exception as e: print(get_error())
                 core.bp_add(user)
             elif hasattr(core, func) and int(rank) > 0:
                 core.bp_add(user)
                 try: self.post(getattr(core, func)(string, user, uid, rank, roomname), roomname)
                 except Exception as e: print(get_error())
         

    def start(self, rooms):
      self.connect(rooms)

    def ping(self, pingid):
      time.sleep(60)
      while self.connected and pingid == self._ping:
        self.send("")
        time.sleep(60)
        
    def pm_ping(self, pingid):
      time.sleep(10)
      while self.connected and pingid == self._ping:
        self.pm_sock.send(b"\r\n\x00")
        time.sleep(10)
                
    def room_auth(self, room):
            ret = 'bauth:' + room + ':' + self._uid + ':' + self.name + ':' + self.password + '\x00'
            return ret

    def send(self,*args):
        if not self.postbyte:
            terminator = b'\x00'
        else:
            terminator = b'\r\n\x00'
            data = ':'.join(args).encode()
            try:
                for item in list(self.sockets.values()):
                    item.send(data+terminator)
            except:
                for x in room_list:
                    self.room_connect(x)

    def pm_send(self,*args):
            terminator = b'\r\n\x00'
            data = ':'.join(args).encode()
            self.pm_sock.send(data+terminator)

    def room_send(self, args, roomname):
        if not self.postbyte:
            terminator = b'\x00'
        else:
            terminator = b'\r\n\x00'
            data = ''.join(args).encode()
            roomsex = ''.join(roomname)
            rsock = self.sockets.get(roomsex)
            rsock.send(data+terminator)

    def ban(self, string, user, uid, rank, roomname):
        if rank > 4 or self.room_level(user, roomname) > 0:
            try:
                ip = self._ips[string]
                mid = self.msg_ids[string]
                self.room_send("block:"+mid+":"+ip+":"+string.lower(), roomname)
                return 'banned: ' + string
            except:
                return 'either there was an error in the command, or i dont have the info to ban the user.'

    def unban(self, string, user, uid, rank, roomname):
        if rank > 4 or self.room_level(user, roomname) > 0:
            try:
                room = ''.join(roomname)
                name = string.lower()
                ip = self.banlist[room+':'+name][1]
                mid = self.banlist[room+':'+name][0]
                self.room_send("removeblock:"+ mid+":"+ ip+":"+ name, roomname)
                return 'unbaned '+name
            except:
                return '%s' % get_error()

    def ban_record(self, users, roomname):
        arguments = users.split('%')
        try:
            name = arguments[2]
            self.banlist.update({''.join(roomname)+':'+name: arguments})
        except: pass            

    def dl(self, string, user, uid, rank, roomname):
        if rank > 1:
            try:
                mid = self.msg_ids[string]
                self.room_send("delallmsg:"+mid, roomname)
                return None
            except: return 'could not delete the message'
        else: return rankError

    def mods(self, string, user, uid, rank, roomname):
        if len(string) == 0: room = ''.join(roomname)
        else: room = string
        try:
            users = self.room_mods[room]
            owner = self.room_owner[room]
            status = core.getUserStatus(users.replace(' ','').split(','))
            status2 = core.getUserStatus(owner)
            shuffle(status)
        except KeyError: return 'not in that room'
        return 'Owner: [%s] Mods: %s' % (''.join(status2),', '.join(status))

    def user_rooms(self, string, user, uid, rank, roomname):
        y = []
        for x in room_list:
            r = self.user_list[x]
            if string.lower() in r: y.append(x)
            else: continue
        return ', '.join(y)

    def add_contact(self, string, user, uid, rank, roomname):
            if ''.join(roomname) == 'way-of-the-nigga':
                    self.pm_send("wladd", string)
                    self.pm_send('wl')

    def get_idle(self, args):
        try:
            name, last_on, is_on, idle = args
            self.idle_dict.update({name: 'online status, '+is_on+', last logged off, ' +core.getSTime(int(last_on))+', idle time: '+ idle_unix(str(idle))})
        except: pass

    def idles(self, string, user, uid, rank, roomname):
            '''needs to be done three times in a row to work if the user is not already in the contacts'''
            self.pm_send('wl')
            try:
                    ret = self.idle_dict[string]
            except KeyError:
                    self.add_contact(string, user, uid, rank, roomname)
                    ret = ''
            return ret

    def lock(self, string, user, uid, rank, roomname):
        if len(string) == 0  and (self._Rank(user, roomname) >= 2 or self.room_level(user, roomname) > 0):
            if ''.join(roomname) not in lockrooms:
                core.rlock(string, user, uid, rank, roomname)
                self.post(user+" locked " + ''.join(roomname), 'way-of-the-nigga')
                return "<font color='#ff0000'><b>LOCKED</b></font>."
            else: return 'already locked'
                
        elif len(string) != 0 and self._Rank(user, roomname) >= 3:
            if string.lower() not in lockrooms:
                core.rlock(string, user, uid, rank, roomname)
                self.post(user+" locked " + string, 'way-of-the-nigga')
                return "%s is now  " % room + "<font color='#ff0000'><b>LOCKED</b></font>."
            else: return 'already locked'

    def unlock(self, string, user, uid, rank, roomname):
        if len(string) == 0  and (self._Rank(user, roomname) >= 2 or self.room_level(user, roomname) > 0):
            if ''.join(roomname) in lockrooms:
                core.runlock(string, user, uid, rank, roomname)
                self.post(user+" unlocked " + ''.join(roomname), ['way-of-the-nigga'])
                return "<font color='#ff0000'><b>UNLOCKED</b></font>."
            else: return 'not locked'
        if len(string) != 0 and self._Rank(user, roomname) >= 3:
            if string.lower() in lockrooms:
                    core.runlock(string, user, uid, rank, roomname)
                    self.post(user+" unlocked " + string, ['way-of-the-nigga'])
                    return "%s is now  " % string + "<font color='#ff0000'><b>UNLOCKED</b></font>."
            else: return 'not locked'

    def post(self, args, roomname, _t = 'n', lang = None):
        #print(args)
        if over_message(args) != True:
            if _t == 'n': ret = self.room_send("bmsg:t12r:"+self.font+args.replace('way-of-the-nigga','chin chin'), roomname)
            elif _t == 'bw': ret = self.room_send("bmsg:t12r:"+self.font+self.escape(args.replace('chin chin','cake')), roomname)
            elif _t == 'l': ret = self.room_send("bmsg:t12r:"+self.font+translate(lang, args), roomname)
        else:
            while len(args) > 0:
                part = args[:2000]
                args = args[2000:]
                ret = self.room_send("bmsg:t12r:"+self.font+part, roomname)
        return ret

    def pm(self, string, user, uid, rank, roomname):
        name, msg = string.split(" ",1)
        self.pm_send('msg',name, 'pm from '+user + ': ' +msg)
        return 'dirk gently'

    def islocked(self, room):
            roomnames = lockrooms
            if room in roomnames: return True
            else: return False


    def flags(self, room):
            if self.ismod(room) and self.islocked(room) == True: return 'b'
            if self.islocked(room): return 'l'
            if self.ismod(room): return 'm'
            else: return 'u'

    def rooms(self, string, user, uid, rank, roomname):
            llist = []
            roomnames = room_list
            for x in roomnames:
                if 'b' in self.flags(x):
                    llist.append('<font color="#ff00ff"><b>'+x+'</b></font>')
                if 'l' in self.flags(x):
                    llist.append('<font color="#ff0000"><b>'+x+'</b></font>')
                elif 'm' in self.flags(x):
                    llist.append('<font color="#0000ff"><b>'+x+'</b></font>')                
                elif 'u' in self.flags(x):
                    llist.append('<font color="#ffffff">'+x+'</font>')
            final = llist
            shuffle(final)
            return 'i am in <font color="#00ffff">%s</font> rooms with <font color="#00ffff">%s</font> users: %s' % (len(room_list), len(ranks.keys()),',  '.join(final))

    def ismod(self, room):
        lvl = self.room_level('illogic', room)
        if lvl > 0: return True
        else: return False

    def record_room(self, string, user):
        joiner = user
        roomname = string.lower()
        roomlock = "unlocked"
        jointime = str(time.time())
        locker = "none"
        locktime = "none"        
        jsonrooms[roomname] = json.dumps([joiner, roomlock, jointime, locker, locktime])
        f = open("jsonrooms.txt", "w")
        for roomname in jsonrooms:
                joiner, roomlock, jointime, locker, locktime = json.loads(jsonrooms[roomname])
                f.write(json.dumps([roomname, joiner, roomlock, jointime, locker, locktime])+"\n")                            
        f.close()

    def uid_record(self, user, uid):
        core.json_record('whois',uids,uid, user)

    def getUid(self, args, line):
           x = []
           uid,names = json.loads(line.strip())
           for name in names.split():
               if name.strip(',') == args:
                   x.append(uid)
           return x 
        
    def whois(self, string, user, uid, rank, roomname):
        x = []
        f = open('whois.txt','r')
        for line in f.readlines():
            uid,names = json.loads(line.strip())
            for xuids in self.getUid(string, line):
                if xuids == uid:
                    x.append("%s: %s" % (uid ,names))
        return '<br><br>' + '<br>'.join(x)

    def room_level(self, user, roomname):
            room = ''.join(roomname)
            try:
                if user == self.room_owner[room]: return 2
            except KeyError: return 0
            if user in self.room_mods[room]: return 1
            else: return 0

    def _Rank(self, user, roomname):
            if user not in ranks:
                if self.room_level(user, roomname) > 0: return 2
                else: return 0
            elif user in ranks:
                    rankname = user
                    ranklvl, rankadder, ranktime = json.loads(ranks[rankname])
                    if self.room_level(user, roomname) >= int(ranklvl):
                        if int(ranklvl) > -1: return 2
                        else: return int(ranklvl)
                    else: return int(ranklvl)

    def reindex(self, string, user, uid, rank, roomname):
        if rank > 5:
            try: list(map(lambda x: exec('imp.reload({a})'.format(a=x.replace('.py','') if x.replace('.py','') != 'cakelib' else 'core')), glob.glob('*.py')))
            except Exception as err: return '{a}'.format(a=err)
            return 'reloaded modules'

    def rank(self, string, user, uid,  rank, roomname):
            if len(string) == 0:
                    lusername = user.lower()
                    if lusername in levelname:
                            lname, lnameo = json.loads(levelname[lusername])
                            lname2 = lname
                    else:
                            if self._Rank(user, roomname) == 1:
                                    lname2 = "a fruitcake"
                            elif self._Rank(user, roomname) == 2:
                                    lname2 = "a gently"
                            elif self._Rank(user, roomname) == 3:
                                    lname2 = "a lemon"
                            elif self._Rank(user, roomname) == 4:
                                    lname2 = "a quark"
                            elif self._Rank(user, roomname) == 5:
                                    lname2 = "an overlord"
                            elif self._Rank(user, roomname) >= 6:
                                    lname2 = "well hung"                            
                    try:
                            rankname = user
                            ranklvl, rankadder, ranktime = json.loads(ranks[rankname])
                            if self.room_level(user, roomname) > 0 and self._Rank(user, roomname) < 2:
                                    return "you are a kunfargle mod: level <font color='#00ffff'>2</font> set by %s %s ago" % (rankadder, core.getSTime(int(ranktime)))
                            else:
                                    return "you are %s: level <font color='#00ffff'>%s</font> set by %s %s ago" % (lname2, str(self._Rank(user, roomname)), rankadder, core.getSTime(int(ranktime)))
                    except:
                            return "you are not yet whitelisted. say *friend (yes just friend, nothing else)"
            elif len(string.split()) == 1:
                    try:
                            rankname = string.lower()
                            ranklvl, rankadder, ranktime = json.loads(ranks[rankname])
                            lusername = string.lower()
                            if lusername in levelname:
                                    lname, lnameo = json.loads(levelname[lusername])
                                    lname2 = lname
                            else:
                                    if int(ranklvl) == 1: lname2 = "a fruitcake"
                                    elif int(ranklvl) == 2: lname2 = "a gently"
                                    elif int(ranklvl) == 3: lname2 = "a lemon"
                                    elif int(ranklvl) == 4: lname2 = "a quark"
                                    elif int(ranklvl) == 5: lname2 = "an overlord"
                                    elif int(ranklvl) >= 6: lname2 = "well hung"
                                    elif int(ranklvl) <= -1: lname2 = "a blacklisted user"
                                    elif int(ranklvl) == 0: lname2 = "a non whitelisted user"
                                    elif int(ranklvl) == None: lname2 = "a non whitelisted user"
                            try:
                                    return "%s is %s: level <font color='#00ffff'>%s</font> set by %s %s ago" % (rankname, lname2, str(self._Rank(rankname, roomname)), rankadder, core.getSTime(int(ranktime)))
                            except:
                                    return "%s is not yet whitelisted" % rankname
                    except:
                            return "%s is not yet whitelisted (<font color='#00ffff'>0</font>)" % string
                        
            elif len(string.split()) == 2:    
                    specials = ["riyoken","why","monday"]
                    try:
                            rankname, ranklvl = string.split(" ", 1)
                            rankadder = user
                            ranktime = time.time()
                            rankname = rankname.lower()
                    except:
                            pass
                    if len(ranklvl) > 0:
                                options = ["-2","-1","0","1","2","3","4","5","6"]
                                if self._Rank(rankname, roomname) != None:
                                        ulvl=int(self._Rank(rankname, roomname))
                                else:
                                        ulvl=int(0)
                                if ulvl > self._Rank(user, roomname) and user not in specials: return "kurnfagle"
                                elif ranklvl not in options and user not in specials: return "that... is not applicable"
                                elif int(ranklvl) == ulvl and user not in specials: return "they are already at that rank."
                                elif ulvl == self._Rank(user, roomname) and int(ranklvl) < ulvl and user not in specials: return "universal lemon"
                                elif self._Rank(user, roomname) > 1 and int(ranklvl) == self._Rank(user, roomname) and user not in specials: return "lemons"
                                elif int(ranklvl) <= 0 and self._Rank(user, roomname) < 2 and user not in specials: return "only rank 2 or above can blacklist or reset"
                                elif int(ranklvl) > self._Rank(user, roomname) and user not in specials: return "necroferger"
                                elif ulvl < 0 and int(ranklvl) > ulvl and self._Rank(user, roomname) < 2 and user not in specials: return "the salmon of doubt"
                                elif rankname == user and user not in specials: return "dirk gently"
                                elif rankname == "riyoken" and user not in specials: return "2000 kilometer long fish in the orbit of jupiter"
                                elif self._Rank(user, roomname) < ulvl and user not in specials: return "slartibartfast"
                                elif rankname == 'dreammist717' and int(ranklvl) < 3: return 'sex me plz :3'
                                elif rankname == 'fancysweetcake' and int(ranklvl) < 3: return 'sex me plz :3'
                                elif int(ranklvl) == 0:
                                    del ranks[rankname]
                                    return 'user is no longer whitelisted'
                                else:
                                        ranks[rankname] = json.dumps([ranklvl, rankadder, ranktime])
                                        f = open("ranks.txt", "w")
                                        for rankname in ranks:
                                                ranklvl, rankadder, ranktime = json.loads(ranks[rankname])
                                                f.write(json.dumps([rankname, ranklvl, rankadder, ranktime])+"\n")
                                        f.close()
                                        rankname, ranklvl = string.split(" ", 1)
                                        self.post("<b>%s</b> ranked %s %s in <b><font color='#ff00ff'>%s</font></b>" % (user, rankname, ranklvl, ''.join(roomname)), ['way-of-the-nigga'])
                                        return "%s is now ranked %s" % (rankname, ranklvl)
            
                    

    def friend(self, string, user, uid, rank, roomname):
            if len(string) == 0: rankname = user
            else: rankname = string
            ranklvl = str(1)
            rankadder = "illogicbot"
            ranktime = time.time()
            rankname = rankname.lower()
            try:
                    derp = ranks[rankname]
                    if len(string) == 0: return 'you are already whitelisted'
                    else: return 'that user is already whitelisted'
            except KeyError:
                    ranks[rankname] = json.dumps([ranklvl, rankadder, ranktime])
                    f = open("ranks.txt", "w")
                    for rankname in ranks:
                        ranklvl, rankadder, ranktime = json.loads(ranks[rankname])
                        f.write(json.dumps([rankname, ranklvl, rankadder, ranktime])+"\n")
                    f.close()
                    if len(string) == 0: return 'welcome to illogicbot :3'
                    else: return 'that user is now whitelisted'
                        
    def lname(self, string, user, uid, rank, roomname):
            if self._Rank(user, roomname) == 1: lnameo = "fruitcake"
            elif self._Rank(user, roomname) == 2: lnameo = "gently"
            elif self._Rank(user, roomname) == 3: lnameo = "lemon"
            elif self._Rank(user, roomname) == 4: lnameo = "quark"
            elif self._Rank(user, roomname) == 5: lnameo = "overlord"
            elif self._Rank(user, roomname) >= 6: lnameo = "well hung"
            lusername = user.lower()
            lname = string
            if len(lname) != 0:
                    levelname[lusername] = json.dumps([lname, lnameo])
                    return "I'll call you " + lname + " then."           
            f = open("levelname.txt", "w")
            for lusername in levelname:
                    lname, lnameo = json.loads(levelname[lusername])
                    f.write(json.dumps([lnameo, lname, lusername])+"\n")
            f.close()

    def bg(self, string, user, uid, rank, roomname):
        if rank > 5:
            for room in room_list:
                self.post(string, list(room))
            return 'sent'
        else: return rankError

    def tc(self, text):
        x = ord(text.group())
        i = html.entities.codepoint2name.get(x)
        if i:
            return '&%s;' % x
        else:
            return '&#%s;' % x
            
    def escape(self, text):
        return re.sub('.', self.tc, text)

    def bw(self, string, user, uid, rank, roomname):
        return self.escape(string)        
        
    def e(self, string, user, uid, rank, roomname):
        if rank > 5:
            if "password" in string.lower():
                return 'fail'
            else:
                try:
                    try: ret = eval(string.decode())
                    except: ret = eval(string)
                    print('eval:', string, roomname)
                    return str(repr(ret))
                except Exception as e:
                    return str('%s' % get_error())
                    return;
        else: return rankError

def clean(x):
    x = x.replace("&lt;", "<")
    x = x.replace("&gt;", ">")
    x = x.replace("&quot;", "\"")
    x = x.replace("&apos;", "'")
    return x

def checkg(x):
        name = x.lower()
        cont = '<table id="group_table"'
        inter = urlreq.urlopen('http://'+name+'.chatango.com').read().decode()
        ret = bool(cont in inter)
        return ret

def idle_unix(x):
        id = int(x)
        if id >= 60:
            id = id/60
            msg = 'hours ago'
            id = str(id).split(".")
            id2 = id[1]
            id = id[0]
            try: id2 = id2[1]
            except: id2 = id2
            id = id + " hours " + id2 + " minutes"
        else:
            id = str(id) + ' minutes'
        return id

def nullstrip(s):
    try: s = s[:s.index('\x00')]
    except ValueError: pass
    return s

def get_error():
        try: et, ev, tb = sys.exc_info()
        except Exception as e: print(e)
        if not tb: return None
        while tb:
                line = tb.tb_lineno
                file = tb.tb_frame.f_code.co_filename
                tb = tb.tb_next
        try: return "%s: %i: %s[%s]" % (file, line, et.__name__, str(ev))
        except Exception as e: print(e)

def over_message(x):
    ret = bool(len(x) > 2500)
    return ret

def anon_id(_id, uid):
  n = _id if _id != None else '3452'
  try: return "".join(str(int(x) + int(y))[-1] for x, y in zip(n, uid[4:]))
  except: return "8===D~"

def getServer(group):
  def n_server(group):
        group = group.replace("_", "q")
        group = group.replace("-", "q")
        lcv10 = min(5, len(group))
        lcv12 = int(group[:lcv10], 36)
        if len(group) > 6:
          lcv11 = group[6:][:min(3, len(group) - 5)]
          lcv8 = int(lcv11, 36)        
        else: lcv8 = 1000
        lcv8 = 1000 if lcv8 <= 1000 or lcv8 == None else lcv8
        num = (lcv12 % lcv8) / lcv8
        limit = sum(map(lambda x: x[1], tagserver_weights))
        cake = 0
        stop = 0
        s_number = 0
        for x in tagserver_weights:
          cake += float(x[1]) / limit
          if(num <= cake) and s_number == 0:
            s_number += int(x[0])
        return s_number
  try:
    sn = str(specials[group])
  except KeyError:
    sn = str(n_server(group))
  return "s{}.chatango.com".format(sn)

def obfus(x):
    x = x.replace('A','7&45*').replace('B','9$%ght').replace('C','#@4hty').replace('O','9$#2ghyer').replace('T','5550gt%').replace('Z','bby&').replace('0','98ty^').replace('I','5*4$')
    return x

def deobfus(x):
    x = x.replace('7&45*','A').replace('9$%ght','B').replace('#@4hty','C').replace('9$#2ghyer','O').replace('5550gt%','T').replace('bby&','Z').replace('98ty^','0').replace('5*4$','I')
    return x

if __name__ == "__main__":
    derp().start(room_list)
